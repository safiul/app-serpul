function SinglePay () {
	const querystring = require('query-string');
	const md5 = require('md5');
	const _ = require('underscore');
	const request = require('request');
	const fs = require('fs');
	const path = require('path');
	const ApiUrl = 'https://202.152.12.106:7227/singlePAY';

	const parsingData = async (product ,responseData) => {
		return new Promise(async (resolve, reject) => {
			let hasil = "";
			let data = responseData.split('\n');
			data.splice(0, 1);
			data.splice(data.length - 1, 1);
			_.each(data, (item, i) => {
				hasil += item+'@';
			});
			console.log(hasil);
			return resolve(hasil);
		});
	}

	// FILTER DATA JENIS LAYANAN
	const getProduk = async (kodeproduk) => {
		return new Promise(async (resolve, reject) => {
			kodeproduk = kodeproduk.toLowerCase();
			let jnsLayanan = "";
			switch(kodeproduk) {
				case "pln" :
					jnsLayanan = "post";
				break;
				case "nontaglis" :
					jnsLayanan = "nebill";
				break;
				default : 
					jnsLayanan = "post";
			}
			return resolve(jnsLayanan);
		});
	}
	// END

	// FILTER DATA JENIS REQUEST
	const getJnsRequest = async (jenis) => {
		return new Promise(async (resolve, reject) => {
			let jnsReq = "";
			switch(jenis) {
				case "5" :
					jnsReq = "inq";
				break;
				case "6" :
					jnsReq = "pay";
				break;
				default :
					jnsReq = "inq";
			}
			return resolve(jnsReq);
		});
	}
	// END

	// GET ARRAYID
	const getArrayID = async (params, jnsLayanan) => {
		return new Promise(async (resolve, reject) => {
			let tujuan = params.tujuan;
			let ArrayID = "";
			switch(jnsLayanan) {
				case "post" : 
					ArrayID = tujuan+'!0!0';
				break;
				case "nebill" :
					ArrayID = tujuan;
				break;
				default : 
					ArrayID = tujuan+'!0!0';
			}
			return resolve(ArrayID);
		});
	}
	// END

	// GET MESSAGE SUKSES INQ/PAY
	const getMsgReq = async (jenis) => {
		return new Promise(async (resolve, reject) => {
			let msg = "";
			switch (jenis) {
				case "5" :
					msg = "CEK TAGIHAN";
				break;
				case "6" :
					msg = "PEMBAYARAN TAGIHAN";
				break;
				default :
					msg = "CEK";
			}
			return resolve(msg);
		});
	}
	// END

	const getOptions = async (req ,JSESSIONID ,params) => {
		return new Promise(async (resolve, reject) => {
			let jenis = params.jenis;
			let tmpIsi = "";
			let jnsReq = await getJnsRequest(params.jenis);
			let jnsLayanan = await getProduk(params.kodeproduk);
			let ArrayID = await getArrayID (params, jnsLayanan);
			tmpIsi = jnsReq+'~'+jnsLayanan+'~'+ArrayID+'~'+JSESSIONID;
			// console.log('tmpIsi ==> '+ tmpIsi);
			tmpIsi = req.generate.enkripsi(tmpIsi);
			let URL = ApiUrl+'/requestmobile?isi='+tmpIsi+'&sogok='+JSESSIONID;
			let cookie = "JSESSIONID="+JSESSIONID+";";
			let options = {
				url : URL,
				method : 'GET',
				headers: {
					'Cookie' : cookie,
				}
			}
			return resolve(options);
		});
	}

	const rqData = async (options) => {
		return new Promise(async ( resolve, reject) => {
			try {
				request(options, (err, response, body) => {
					if (!err && response.statusCode == 200) {
						if (body) {
							let info = JSON.parse(body);
							return resolve(info);
						} else {
							return reject("ERRCODE:203 REQUEST GAGAL, DATA KONTEN KOSONG");
						}
					} else {
						return reject(err);
					}
				});
			} catch (err) {
				return reject(err);
			}
		});
	}

	const rollback = async (req, params, callback) => {
		let tujuan = params.tujuan;
		let password = "123"// params.pass;
		let username = "noer"; //params.pin
		let tmpUserLogin = req.generate.enkripsi(username+'~9e0d031d6908a681~1.0.0~andro');
		let tmpPswdLogin = md5(password);
		let tmpSogokLogin = md5(tmpUserLogin+"SUBHANALLAH-ALHAMDULILLAH");
		let URLlogin = ApiUrl+"/loginmobile?act=login&usr="+tmpUserLogin+"&psw="+tmpPswdLogin+"&sogok="+tmpSogokLogin;
		let optionsLogin = {
			url: URLlogin,
			method : 'GET',
		}
		request(optionsLogin, async (err, response, body) => {
			if (!err && response.statusCode == 200) {
				let infoLogin = JSON.parse(body);
				console.log('hasil dari rollback');
				console.log(infoLogin);
				if (infoLogin.rc == "00") {
					let setcookie = response.headers["set-cookie"][0];
					let query = querystring.parse(setcookie);
					let JSESSIONID = query.JSESSIONID;
					let pecah = JSESSIONID.split(';');
					let session = pecah[0];
					// console.log('get session ===> '+ session);
					req.logger.log('info', 'request login ulang sukses pin : '+params.pin+' tujuan : '+tujuan+' JSESSIONID : '+ session)
					req.db.update('jsessionid', {_id : username }, { $set : { JSESSIONID : session }} , (err, rows) => {
					});
					let optionsRequest = await getOptions(req, session, params);
					return callback(null,optionsRequest);
				} else {
					req.logger.log('error', 'request login gagal');
					req.logger.log('error', info.data+" pin:"+params.pin+" tujuan:"+tujuan+" pass:"+params.pass);
					return callback(infoLogin.data+"  tujuan:"+tujuan , null);
				}
			} else {
				req.logger.log('error', 'request login gagal');
				req.logger.log('error', err);
				return callback('ERRCODE:000 REQUEST SESSION GAGAL, SILAHKAN DIULANGI  tujuan:'+tujuan, null);
			}
		});
	}

	this.isLogin = (req, res, next) => {
		let b = req.query;
		const url = req.url;
		req.logger.log('info', url);
		req.logger.log('info', b);
		if (b.pin == undefined || b.pin == "" ) {
			req.logger.log('error', 'ERRCODE:404 REQUEST DITOLAK, DATA PIN TIDAK DI KENALI / KOSONG  PIN:'+b.pin);
			req.logger.log('error', url);
			return res.send('ERRCODE:404 REQUEST DITOLAK, DATA PIN TIDAK DI KENALI / KOSONG  PIN:'+b.pin);
		}

		if (b.tujuan == undefined || b.tujuan == "") {
			req.logger.log('error', 'ERRCODE:404 REQUEST DITOLAK, DATA TUJUAN TIDAK DI KENALI / KOSONG  IDPEL:'+b.tujuan);
			req.logger.log('error', url);
			return res.send('ERRCODE:404 REQUEST DITOLAK, DATA TUJUAN TIDAK DI KENALI / KOSONG  IDPEL:'+b.tujuan);
		}

		if (b.pass == undefined || b.pass == "") {
			req.logger.log('error', 'ERRCODE:404 REQUEST DITOLAK, PASSWORD TIDAK DI KENALI / KOSONG  PASS:'+b.pass);
			req.logger.log('error', url);
			return res.send('ERRCODE:404 REQUEST DITOLAK, PASSWORD TIDAK DI KENALI / KOSONG  PASS:'+b.pass);
		}

		if (b.kodeproduk == undefined || b.kodeproduk == "") {
			req.logger.log('error', 'ERRCODE:404 REQUEST DITOLAK, KODE PRODUK TIDAK DI KENALI / KOSONG  KODEPRODUK:'+b.kodeproduk);
			req.logger.log('error', url);
			return res.send('ERRCODE:404 REQUEST DITOLAK, KODE PRODUK TIDAK DI KENALI / KOSONG  KODEPRODUK:'+b.kodeproduk);
		}

		if (b.jenis == undefined || b.jenis == "") {
			req.logger.log('error', 'ERRCODE:404 REQUEST DITOLAK, JENIS REQUEST TIDAK DI KENALI / KOSONG  JENIS:'+b.jenis);
			req.logger.log('error', url);
			return res.send('ERRCODE:404 REQUEST DITOLAK, JENIS REQUEST TIDAK DI KENALI / KOSONG  JENIS:'+b.jenis);
		}

		let password = '123';//b.pass;
		let username = 'noer';//b.pin;
		let tujuan = b.tujuan;
		req.db.findOne('jsessionid', { _id : username }, (err, docs) => {
			if (docs == null) {
				console.log('login ke getway singlePAY');
				let tmpUser = username+'~9e0d031d6908a681~1.0.0~andro';
				console.log(tmpUser);
				tmpUser = req.generate.enkripsi(tmpUser);
				let tmpPswd = md5(password);
				let tmpSogok = md5(tmpUser+"SUBHANALLAH-ALHAMDULILLAH");
				let URL = ApiUrl+"/loginmobile?act=login&usr="+tmpUser+"&psw="+tmpPswd+"&sogok="+tmpSogok;
				let options = {
					url: URL,
					method : 'GET',
				};
				console.log('URL : '+ URL);
				req.logger.log('info','login ke getway singlePAY');
				request(options, (err, response, body) => {
					if (!err && response.statusCode == 200) {
						if (body) {
							let info = JSON.parse(body);
							console.log(info);
							req.logger.log('info', info);
							if (info.rc == "00") {
								let setcookie = response.headers["set-cookie"][0];
								let params = querystring.parse(setcookie);
								let JSESSIONID = params.JSESSIONID;
								let pecah = JSESSIONID.split(';');
								let session = pecah[0];
								let dataInsert = {
									_id : username,
									password : password,
									JSESSIONID : session,
								}
								req.logger.log('info', 'request login sukses pin : '+b.pin+' tujuan : '+tujuan+' JSESSIONID : '+ session)
								req.db.SaveData('jsessionid', dataInsert, (err, rows) => {
									req['JSESSIONID'] = session;
									return next();
								});
							} else {
								req.logger.log('error', 'request login gagal');
								req.logger.log('error', info.data);
								return res.send(info.data+' IDPEL:'+tujuan+'@RC:'+info.rc);
							}
						} else {
							req.logger.log('error', 'request login gagal');
							req.logger.log('error', 'response dari getway kosong');
							return res.send('ERRCODE:203 REQUEST GAGAL, DATA KONTEN KOSONG ');
						}
					} else {
						req.logger.log('error', 'request login gagal');
						req.logger.log('error', err);
						return res.send("ERRCODE:000 REQUEST SESSION GAGAL, SILAHKAN DIULANGI  tujuan:"+tujuan);
					}
				});
			} else {
				console.log('JSESSIONID SUDAH ADA DI DATABASE');
				req['JSESSIONID'] = docs.JSESSIONID;
				return next();
			}
		});
	}

	this.requestDataGet = async (req, res, next) => {
		let b = req.query;
		let kodeproduk = b.kodeproduk;
		let tujuan = b.tujuan;
		let idtrx = b.idtrx;
		let jenis = b.jenis;
		let counter = b.counter;
		let user = b.user;
		let pass = b.pass;
		let id = b.id;
		const JSESSIONID = req.JSESSIONID;
		if (JSESSIONID == undefined ) {
			req.logger.log('error', "SESSION SUDAH USANG tujuan:"+tujuan);
			return res.send('ERRCODE:403  SESSION TIDAK DI KENALI, SILAHKAN INQUERY ULANG  tujuan:'+tujuan);
		} else {
			let options = await getOptions(req, JSESSIONID, b); // get options paramater
			let msgReq = await getMsgReq(jenis);
			rqData(options)
				.then(async (info) => {
					console.log('hasil request pertama');
					console.log(info);
					if (info.rc == "00") {
						req.logger.log('info', "request sukses tujuan:"+tujuan);
						req.logger.log('info', msgReq+' '+b.kodeproduk.toUpperCase()+' SUKSES IDPEL:'+tujuan+'@NAMA:NY KUSTIYAH@TD:-@BLN:APR2018@JML:1@SM:00024550-00025338@TAGASLI:1254714@ADM:2000@TAG:1256714@')
						return res.send(msgReq+' '+b.kodeproduk.toUpperCase()+' SUKSES IDPEL:'+tujuan+'@NAMA:NY KUSTIYAH@TD:-@BLN:APR2018@JML:1@SM:00024550-00025338@TAGASLI:1254714@ADM:2000@TAG:1256714@');
					} else if (info.rc == "999") {
						req.logger.log('error', msgReq+" "+b.kodeproduk.toUpperCase()+" GAGAL, tujuan:"+tujuan);
						req.logger.log('error', info.data);
						if (jenis == "5") {
							rollback(req, b, async (err, resOtpions) => {
								if (err) return res.send(err);
								rqData(resOtpions).then(async (output) => {
									console.log('log terakhir ');
									req.logger.log('info', "request ulang sukses tujuan:"+tujuan);
									req.logger.log('info', msgReq+' '+b.kodeproduk.toUpperCase()+' SUKSES IDPEL:'+tujuan+'@NAMA:NY KUSTIYAH@TD:-@BLN:APR2018@JML:1@SM:00024550-00025338@TAGASLI:1254714@ADM:2000@TAG:1256714@')
									return res.send(msgReq+' '+b.kodeproduk.toUpperCase()+' SUKSES IDPEL:'+tujuan+'@NAMA:NY KUSTIYAH@TD:-@BLN:APR2018@JML:1@SM:00024550-00025338@TAGASLI:1254714@ADM:2000@TAG:1256714@');
								})
								.catch(async (err) => {
									req.logger.log('error', "ERRCODE:001 REQUEST GAGAL, TERJADI KESALAHAN tujuan:"+tujuan)
									return res.send(tujuan+"  ERRCODE:001 REQUEST GAGAL, TERJADI KESALAHAN");
								});
							})
						} else {
							req.logger.log('error', "SESSION SUDAH USANG tujuan:"+tujuan);
							return res.send(tujuan+' ERRCODE:403  SESSION TIDAK DI KENALI, SILAHKAN INQUERY ULANG');
						}
					} else {
						let message = "";
						let rc = info.rc;
						if (info.data == "null" || info.data == "") {
							req.logger.log('error', "ERRCODE:404 NOMOR METER/IDPEL BELUM TERSEDIA tujuan:"+tujuan);
							message = "ERRCODE:404 NOMOR METER/IDPEL BELUM TERSEDIA";
						} else {
							req.logger.log('error', info.data+" tujuan:"+tujuan);
							message = info.data;
						}
						return res.send(message+' IDPEL:'+b.tujuan+'@RC:'+rc+'@');
					}
				})
				.catch(async (err) => {
					console.log('ERROR 1', err);
					req.logger.log('error', "ERRCODE:001 REQUEST GAGAL, TERJADI KESALAHAN tujuan:"+tujuan)
					return res.send("ERRCODE:001 REQUEST GAGAL, TERJADI KESALAHAN  tujuan:"+tujuan);
				})
		}
	}
}

module.exports = SinglePay;
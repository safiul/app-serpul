// http://203.89.25.34:8888/trx?memberID=<ID>&product=<kode>&dest=<tujuan>&refID=<idtrx>&pin=<PIN>&password=<PASS>
function ServiceHandler () {
	const querystring = require('query-string');
	const _ = require('underscore');
	const md5 = require('md5');
	const request = require('request');
	const ApiUrl = 'https://202.152.12.106:7227/singlePAY';
	
	this.isAuth = async (req, res, next) => {
		const url = req.url;
		const b = req.query;
		console.log(url);
		req.logger.log('info', url);
		req.logger.log('info', b);
		if (b.pin == undefined || b.pin == "" ) {
			req.logger.log('error', 'ERRCODE:404 REQUEST DITOLAK, DATA PIN TIDAK DI KENALI / KOSONG  PIN:'+b.pin);
			req.logger.log('error', url);
			return res.send('ERRCODE:404 REQUEST DITOLAK, DATA PIN TIDAK DI KENALI / KOSONG  PIN:'+b.pin);
		}

		if (b.dest == undefined || b.dest == "") {
			req.logger.log('error', 'ERRCODE:404 REQUEST DITOLAK, DATA DEST TIDAK DI KENALI / KOSONG  IDPEL:'+b.dest);
			req.logger.log('error', url);
			return res.send('ERRCODE:404 REQUEST DITOLAK, DATA DEST TIDAK DI KENALI / KOSONG  IDPEL:'+b.dest);
		}

		if (b.password == undefined || b.password == "") {
			req.logger.log('error', 'ERRCODE:404 REQUEST DITOLAK, PASSWORD YANG TIDAK DI KENALI / KOSONG  PASS:'+b.password);
			req.logger.log('error', url);
			return res.send('ERRCODE:404 REQUEST DITOLAK, PASSWORD YANG TIDAK DI KENALI / KOSONG  PASS:'+b.password);
		}

		if (b.product == undefined || b.product == "") {
			req.logger.log('error', 'ERRCODE:404 REQUEST DITOLAK, KODE PRODUK TIDAK DI KENALI / KOSONG  KODEPRODUK:'+b.product);
			req.logger.log('error', url);
			return res.send('ERRCODE:404 REQUEST DITOLAK, KODE PRODUK TIDAK DI KENALI / KOSONG  KODEPRODUK:'+b.product);
		}

		let username = 'noer';
		let password = '123';
		let dest = b.dest;
		req.db.findOne('jsessionid', { _id : username }, (err, docs) => {
			if (err) {
				req.logger.log('error', err);
				req.logger.log('error', 'query jsessionid di database gagal');
				return res.send('ERRCODE:502 REQUEST GAGAL, SILAHKAN ULANGI ');
			}

			if (docs == null) {
				console.log('login ke getway singlePAY');
				let tmpUser = req.generate.enkripsi(username+'~9e0d031d6908a681~1.0.0~andro');
				let tmpPswd = md5(password);
				let tmpSogok = md5(tmpUser+"SUBHANALLAH-ALHAMDULILLAH");
				let URL = ApiUrl+"/loginmobile?act=login&usr="+tmpUser+"&psw="+tmpPswd+"&sogok="+tmpSogok;
				let options = {
					url: URL,
					method : 'GET',
				};
				console.log('URL : '+ URL);
				req.logger.log('info','login ke getway singlePAY untuk product : '+b.product);
				request(options, (err, response, body) => {
					if (!err && response.statusCode == 200) {
						if (body) {
							let info = JSON.parse(body);
							req.logger.log('info', info);
							console.log(info);
							if (info.rc == "00") {
								let setcookie = response.headers["set-cookie"][0];
								let params = querystring.parse(setcookie);
								let JSESSIONID = params.JSESSIONID;
								let pecah = JSESSIONID.split(';');
								let session = pecah[0];
								let dataInsert = {
									_id : username,
									password : password,
									JSESSIONID : session,
								}
								req.logger.log('info', 'request login sukses pin : '+b.pin+' dest : '+dest+' JSESSIONID : '+ session+" untuk product : "+b.product);
								req.db.SaveData('jsessionid', dataInsert, (err, rows) => {
									req['JSESSIONID'] = session;
									return next();
								});
							} else {
								req.logger.log('error', 'request login gagal untuk product:'+b.product+" dest:"+dest);
								req.logger.log('error', info.data);
								return res.send(info.data+' IDPEL:'+dest+'@RC:'+info.rc);
							}
						} else {
							req.logger.log('error', 'request login gagal untuk pin:'+b.pin+" dest:"+dest+" product:"+b.product);
							req.logger.log('error', 'response dari getway kosong');
							return res.send('ERRCODE:203 REQUEST GAGAL, DATA KONTEN KOSONG ');
						}
					} else {
						req.logger.log('error', 'request session gagal untuk pin:'+b.pin+" dest:"+dest+" product:"+b.product);
						req.logger.log('error', err);
						return res.send("ERRCODE:000 REQUEST SESSION GAGAL, SILAHKAN DIULANGI  dest:"+dest);
					}
				})
			} else {
				req.logger.log('info', "JSESSIONID SUDAH ADA DI DATABASE "+docs.JSESSIONID)
				console.log('JSESSIONID SUDAH ADA DI DATABASE');
				req['JSESSIONID'] = docs.JSESSIONID;
				return next();
			}
		});
	}

	this.reqDataGet = async (req, res, next) => {
		const JSESSIONID = req.JSESSIONID;
		const b = req.query;
		let product = b.product;
		let dest = b.dest;
		let idtrx = b.idtrx;
		let pin = b.pin;
		let password = b.password;
		let memberID = b.memberID;
		if (JSESSIONID == undefined ) {
			req.logger.log('error', "SESSION SUDAH USANG dest:"+dest);
			return res.send('ERRCODE:403  SESSION TIDAK DI KENALI, SILAHKAN INQUERY ULANG  dest:'+dest);
		} else {
			let options = await getOptions(req, JSESSIONID, b);
			let msgReq = await getMsgReq(product);
			reqData(options)
				.then(async (info) => {
					console.log('hasil request pertama');
					if (info.rc == "00") {
						req.logger.log('info', "request inquery/payment sukses dest:"+dest);
						req.logger.log('info', info.data);
						req.logger.log('info', msgReq+' IDPEL:'+dest+'@NAMA:NY KUSTIYAH@TD:-@BLN:APR2018@JML:1@SM:00024550-00025338@TAGASLI:1254714@ADM:2000@TAG:1256714@')
						// FILTER RESPONSE MSG
						if (product == "cekpln") {
							let output =  await parsingData(product, info.data);
							return res.send(msgReq+' '+output);
						} else {
							return res.send(msgReq+' IDPEL:'+dest+'@NAMA:NY KUSTIYAH@TD:-@BLN:APR2018@JML:1@SM:00024550-00025338@TAGASLI:1254714@ADM:2000@TAG:1256714@');
						}
					} else if (info.rc == "999") {
						req.logger.log('error', msgReq+" GAGAL dest:"+dest);
						req.logger.log('error', info.data);
						rollback(req, b, async (err, resOtpions) => {
							if (err) return res.send(err);
							reqData(resOtpions)
								.then(async (output) => {
									req.logger.log('info', "request ulang sukses dest:"+dest);
									// FILTER RESPONSE MSG
									let outMsg =  await parsingData(product, output.data);
									if (product == "cekpln") {
										req.logger.log('info', msgReq+' '+outMsg);
										return res.send(msgReq+' '+outMsg);
									} else {
										req.logger.log('info', msgReq+' IDPEL:'+dest+'@NAMA:NY KUSTIYAH@TD:-@BLN:APR2018@JML:1@SM:00024550-00025338@TAGASLI:1254714@ADM:2000@TAG:1256714@');
										return res.send(msgReq+' IDPEL:'+dest+' @NAMA:NY KUSTIYAH@TD:-@BLN:APR2018@JML:1@SM:00024550-00025338@TAGASLI:1254714@ADM:2000@TAG:1256714@');
									}
								})
								.catch((err) => {
									console.log('ERROR 1', err);
									req.logger.log('error', "ERRCODE:001 REQUEST ULANG GAGAL, TERJADI KESALAHAN dest:"+dest)
									req.logger.log('error', err);
									return res.send("ERRCODE:001 REQUEST GAGAL, TERJADI KESALAHAN dest:"+dest);
								})
						});
					} else {
						let message = "";
						let rc = info.rc;
						if (info.data == "null" || info.data == "") {
							req.logger.log('error', "ERRCODE:404 NOMOR METER/IDPEL BELUM TERSEDIA dest:"+dest);
							req.logger.log('error', info);
							message = dest+"  ERRCODE:404 NOMOR METER/IDPEL BELUM TERSEDIA";
						} else {
							req.logger.log('error', info.data+" dest:"+dest);
							message = info.data;
						}
						return res.send(message+' IDPEL:'+b.dest+'@RC:'+rc+'@');
					}
				})
				.catch((err) => {
					console.log('ERROR 1', err);
					req.logger.log('error', "ERRCODE:001 REQUEST GAGAL, TERJADI KESALAHAN dest:"+dest)
					req.logger.log('error', err);
					return res.send("ERRCODE:001 REQUEST GAGAL, TERJADI KESALAHAN dest:"+dest);
				})
		}
	}

	// REQUEST ULANG JIKA RESPONSE RC 999
	const rollback = async (req, params, callback) => {
		let dest = params.dest;
		let password = "123"// params.password;
		let username = 'noer'; // params.pin;
		let tmpUserLogin = req.generate.enkripsi(username+'~9e0d031d6908a681~1.0.0~andro');
		let tmpPswdLogin = md5(password);
		let tmpSogokLogin = md5(tmpUserLogin+"SUBHANALLAH-ALHAMDULILLAH");
		let URLlogin = ApiUrl+"/loginmobile?act=login&usr="+tmpUserLogin+"&psw="+tmpPswdLogin+"&sogok="+tmpSogokLogin;
		let optionsLogin = {
			url: URLlogin,
			method : 'GET',
		}
		request(optionsLogin, async (err, response, body) => {
			if (!err && response.statusCode == 200) {
				let infoLogin = JSON.parse(body);
				console.log('hasil dari rollback');
				console.log(infoLogin);
				if (infoLogin.rc == "00") {
					let setcookie = response.headers["set-cookie"][0];
					let query = querystring.parse(setcookie);
					let JSESSIONID = query.JSESSIONID;
					let pecah = JSESSIONID.split(';');
					let session = pecah[0];
					console.log('get session ulang ===> '+ session);
					req.logger.log('info', 'request login ulang sukses pin : '+params.pin+' dest : '+dest+' JSESSIONID : '+ session+" untuk product : "+params.product);
					req.db.update('jsessionid', {_id : username }, { $set : { JSESSIONID : session }} , (err, rows) => {
					});
					let optionsRequest = await getOptions(req, session, params);
					return callback(null,optionsRequest);
				} else {
					req.logger.log('error', 'request login ulang gagal');
					req.logger.log('error', infoLogin.data+" pin:"+params.pin+" dest:"+dest+" password:"+params.password);
					return callback(infoLogin.data+" dest:"+dest , null);
				}
			} else {
				req.logger.log('error', 'request login ulang gagal dest:'+dest);
				req.logger.log('error', err);
				return callback('ERRCODE:000 REQUEST SESSION GAGAL, SILAHKAN DIULANGI dest:'+dest, null);
			}
		});
	}
	// END

	const parsingData = async (product ,responseData) => {
		return new Promise(async (resolve, reject) => {
			let hasil = "";
			let data = responseData.split('\n');
			data.splice(0, 1);
			data.splice(data.length - 1, 1);
			_.each(data, (item, i) => {
				hasil += item+'@';
			});
			return resolve(hasil);
		});
	}

	const reqData = async (options) => {
		return new Promise(async ( resolve, reject) => {
			try {
				request(options, (err, response, body) => {
					if (!err && response.statusCode == 200) {
						if (body) {
							let info = JSON.parse(body);
							return resolve(info);
						} else {
							return reject("ERRCODE:203 REQUEST GAGAL, DATA KONTEN KOSONG");
						}
					} else {
						return reject(err);
					}
				});
			} catch (err) {
				return reject(err);
			}
		});
	}

	const getOptions = async (req ,JSESSIONID ,params) => {
		return new Promise (async (resolve, reject) => {
			let tmpIsi = "";
			let jnsReq = await getJnsRequest(params.product);
			let ArrayID = await getArrayID (params, params.product);
			tmpIsi = jnsReq+'~'+ArrayID+'~'+JSESSIONID;
			tmpIsi = req.generate.enkripsi(tmpIsi);
			let URL = ApiUrl+'/requestmobile?isi='+tmpIsi+'&sogok='+JSESSIONID;
			let cookie = "JSESSIONID="+JSESSIONID+";";
			let options = {
				url : URL,
				method : 'GET',
				headers: {
					'Cookie' : cookie,
				}
			}
			return resolve(options);
		});
	}

	// GET JENIS REQUEST LAYANAN PRODUK
	const getJnsRequest = async (product) => {
		return new Promise (async (resolve, reject) => {
			let jnsReq = "";
			switch(product.toLowerCase()) {
				case "cekpln" :
					jnsReq = "inq~post";
				break;
				case "pln" :
					jnsReq = "pay~post";
				break;
				case "ceknontaglis" :
					jnsReq = "inq~nebill";
				break;
				case "nontaglis" :
					jnsReq = "pay~nebill";
				break;
			}
			return resolve(jnsReq);
		});
	}
	// END

	// GET ARRAYID
	const getArrayID = async (params, product) => {
		return new Promise (async (resolve, reject) => {
			let ArrayID = "";
			let dest = params.dest;
			switch(product.toLowerCase()) {
				case "cekpln" :
					ArrayID = dest+'!0!0';
				break;
				case "pln" :
					ArrayID = dest;
				break;
				case "ceknontaglis" :
					ArrayID = dest;
				break;
				case "nontaglis" :
					ArrayID = dest;
				break;
			}
			return resolve(ArrayID);
		});
	}
	// END

	// GET MESSAGE SUKSES INQ/PAY
	const getMsgReq = async (product) => {
		return new Promise(async (resolve, reject) => {
			let msg = "";
			switch (product.toLowerCase()) {
				case "cekpln" :
					msg = "CEK TAGIHAN PLN SUKSES";
				break;
				case "pln" :
					msg = "PEMBAYARAN TAGIHAN PLN SUKSES";
				break;
				case "ceknontaglis" :
					msg = "CEK TAGIHAN NONTAGLIS SUKSES";
				break;
				case "nontaglis" :
					msg = "PEMBAYARAN TAGIHAN NONTAGLIS SUKSES";
				break;
				default :
					msg = "CEK TAGIHAN PLN SUKSES";
				break;
			}
			return resolve(msg);
		});
	}
	// END
}

module.exports  = ServiceHandler;
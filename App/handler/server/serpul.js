function Serpul () {
	const mode = process.env.NODE_ENV || 'dev';
	var configApp = require('../../config/app.json')[mode] || require('../App/config/app.json')['dev'];
	const querystring = require('query-string');
	const _ = require('underscore');
	const request = require('request');
	const md5 = require('md5');

	// const ApiUrl = 'https://192.168.0.3:7227/singlePAY'; ===> DEVEL
	// const ApiUrl = 'https://202.152.12.106:7227/singlePAY';
	//'https://192.168.0.10:8443/singlePAY'; ====> PRODUCTION
	
	const ApiUrl = configApp.app.ApiUrl;

	this.isAuth = async (req, res, next) => {
		const url = req.url;
		const b = req.query;
		console.log(url);
		req.logger.log('info', url);
		req.logger.log('info', b);

		if (b.pin == undefined || b.pin == "" ) {
			req.logger.log('error', 'ERRCODE:404 REQUEST DITOLAK, DATA PIN TIDAK DI KENALI / KOSONG  PIN:'+b.pin);
			req.logger.log('error', url);
			return res.send('ERRCODE:404 REQUEST DITOLAK, DATA PIN TIDAK DI KENALI / KOSONG  PIN:'+b.pin);
		}

		if (b.dest == undefined || b.dest == "") {
			req.logger.log('error', 'ERRCODE:404 REQUEST DITOLAK, DATA DEST TIDAK DI KENALI / KOSONG  IDPEL:'+b.dest);
			req.logger.log('error', url);
			return res.send('ERRCODE:404 REQUEST DITOLAK, DATA DEST TIDAK DI KENALI / KOSONG  IDPEL:'+b.dest);
		}

		if (b.password == undefined || b.password == "") {
			req.logger.log('error', 'ERRCODE:404 REQUEST DITOLAK, PASSWORD YANG TIDAK DI KENALI / KOSONG  PASS:'+b.password);
			req.logger.log('error', url);
			return res.send('ERRCODE:404 REQUEST DITOLAK, PASSWORD YANG TIDAK DI KENALI / KOSONG  PASS:'+b.password);
		}

		if (b.product == undefined || b.product == "") {
			req.logger.log('error', 'ERRCODE:404 REQUEST DITOLAK, PRODUK TIDAK DI KENALI / KOSONG  PRODUCT:'+b.product);
			req.logger.log('error', url);
			return res.send('ERRCODE:404 REQUEST DITOLAK, PRODUK TIDAK DI KENALI / KOSONG  PRODUCT:'+b.product);
		}

		let username = b.pin; //'noer';
		let password = b.password; //'123';
		let dest = b.dest;
		req.db.findOne('jsessionid', { _id : username }, (err, docs) => {
			if (err) {
				req.logger.log('error', err);
				req.logger.log('error', 'query jsessionid di database gagal');
				return res.send('ERRCODE:502 REQUEST GAGAL, SILAHKAN ULANGI ');
			}

			if (docs == null) {
				console.log('login ke getway singlePAY');
				let tmpUser = req.generate.enkripsi(username+'~9e0d031d6908a681~1.0.0~andro');
				let tmpPswd = md5(password);
				let tmpSogok = md5(tmpUser+"SUBHANALLAH-ALHAMDULILLAH");
				let URL = ApiUrl+"/logingatewayserpul?act=login&usr="+encodeURI(tmpUser)+"&psw="+tmpPswd+"&sogok="+tmpSogok;
				console.log(URL);
				
				let options = {
					url: URL,
					method : 'GET',
				};
				console.log('URL : '+ URL);
				req.logger.log('info','login ke getway singlePAY untuk product : '+b.product);
				request(options, (err, response, body) => {
					if (!err && response.statusCode == 200) {
						if (body) {
							let info = JSON.parse(body);
							req.logger.log('info', info);
							console.log(info);
							if (info.rc == "00") {
								let setcookie = response.headers["set-cookie"][0];
								let params = querystring.parse(setcookie);
								let JSESSIONID = params.JSESSIONID;
								let pecah = JSESSIONID.split(';');
								let session = pecah[0];
								let dataInsert = {
									_id : username,
									password : password,
									JSESSIONID : session,
								}
								req.logger.log('info', 'request login sukses pin : '+b.pin+' dest : '+dest+' JSESSIONID : '+ session+" untuk product : "+b.product);
								req.db.SaveData('jsessionid', dataInsert, (err, rows) => {
									req['JSESSIONID'] = session;
									return next();
								});
							} else {
								req.logger.log('error', 'request login gagal untuk product:'+b.product+" dest:"+dest);
								req.logger.log('error', info.data);
								return res.send(info.data+' IDPEL:'+dest+'@RC:'+info.rc);
							}
						} else {
							req.logger.log('error', 'request login gagal untuk pin:'+b.pin+" dest:"+dest+" product:"+b.product);
							req.logger.log('error', 'response dari getway kosong');
							return res.send('ERRCODE:203 REQUEST GAGAL, DATA KONTEN KOSONG ');
						}
					} else {
						req.logger.log('error', 'request session gagal untuk pin:'+b.pin+" dest:"+dest+" product:"+b.product);
						req.logger.log('error', err);
						return res.send("ERRCODE:000 REQUEST SESSION GAGAL, SILAHKAN DIULANGI  dest:"+dest);
					}
				})
			} else {
				req.logger.log('info', "JSESSIONID SUDAH ADA DI DATABASE "+docs.JSESSIONID)
				console.log('JSESSIONID SUDAH ADA DI DATABASE');
				req['JSESSIONID'] = docs.JSESSIONID;
				return next();
			}
		});
	}

	this.index = async (req, res, next) => {
		const JSESSIONID = req.JSESSIONID;
		const b = req.query;
		console.log(b);
		let product = b.product;
		product = product.toLowerCase();
		let dest = b.dest;
		let idtrx = b.idtrx;
		let pin = b.pin;
		let password = b.password;
		let memberID = b.memberID;
		if (JSESSIONID == undefined ) {
			req.logger.log('error', "SESSION SUDAH USANG dest:"+dest);
			return res.send('ERRCODE:403  SESSION TIDAK DI KENALI, SILAHKAN INQUERY ULANG  dest:'+dest);
		} else {
			// let arr_kode = ['cekpln','pln','ceknontaglis','nontaglis','cekprepaid','cekbpjskes','bpjskes','cekfinance','finance','cektelcopos','telcopos','cektelcopre','telcopre'];
			let arr_prod = product.split('!');
			let arr_kode = ['cekpln','pln','ceknontaglis','nontaglis','cekprepaid'];
			let cek = arr_kode.indexOf(product);
			console.log("CEK ===> "+ cek);
			let cek_kode = product.search('plnpre');
			console.log("cek_kode ===> "+ cek_kode);
			// INI UNTUK REQUEST PLN PREPAID
			if (cek_kode == 0) {
				console.log('PAYMENT PREPAID')
				let options = await getOptions(req, JSESSIONID , {product : 'cekprepaid', dest : dest, kode : product});
				// console.log(options);
				reqData(options)
					.then(async (info) => {
						console.log(info);
						if (info.rc == "00") {
							let parseData = info.data.split('|');
							let nometer = parseData[0];
							let optionsPay = await getOptions(req, JSESSIONID, {product : 'prepaid', dest : nometer, kode : product });
							request(optionsPay, async (err, response, body) => {
								if (!err && response.statusCode == 200) {
									if (body) {
										let msgReq = await getMsgReq("prepaid");
										let infoPay = JSON.parse(body);
										if (infoPay.rc == "00") {
											let msgValue = await pilahData('prepaid', infoPay.data);
											return res.send(msgReq+" "+msgValue);
										} else {
											let message = "";
											let rc = infoPay.rc;
											if (infoPay.data == "null" || infoPay.data == "") {
												req.logger.log('error', "ERRCODE:404 NOMOR METER/IDPEL BELUM TERSEDIA dest:"+dest);
												req.logger.log('error', infoPay);
												message = "ERRCODE:404 NOMOR METER/IDPEL BELUM TERSEDIA";
											} else {
												req.logger.log('error', infoPay.data+" dest:"+dest);
												message = infoPay.data;
											}
											return res.send(message+' IDPEL:'+b.dest+'@RC:'+rc+'@');
										}
									} else {
										req.logger.log('error', 'hasil request kosong gagal untuk pin:'+b.pin+" dest:"+dest+" product:"+b.product);
										req.logger.log('error', 'response dari getway kosong');
										return res.send('ERRCODE:203 REQUEST GAGAL, DATA KONTEN KOSONG ');
									}
								} else {
									req.logger.log('error', "ERRCODE:001 REQUEST GAGAL, TERJADI KESALAHAN dest:"+dest)
									req.logger.log('error', err);
									return res.send("ERRCODE:001 REQUEST GAGAL, TERJADI KESALAHAN dest:"+dest);
								}
							})
						} else if (info.rc == "999") {
							let opt = {
								dest : dest,
								product : 'cekprepaid',
								pin : pin,
								password : password,
								flag : true,
							}
							rollback(req, opt, async (err, resOtpions) => {
								if (err) return res.send(err);
								let SESSIONID = resOtpions.JSESSIONID;
								reqData(resOtpions.options)
									.then(async (output) => {
										if (output.rc == "00") {
											let parseData = output.data.split('|');
											let nometer = parseData[0];
											let optionsPay = await getOptions(req, SESSIONID, {product : 'prepaid', dest : nometer, kode : product });
											request(optionsPay, async (err, response, body) => {
												if (!err && response.statusCode == 200) {
													if (body) {
														let msgReq = await getMsgReq("prepaid");
														let infoPay = JSON.parse(body);
														if (infoPay.rc == "00") {
															let msgValue = await pilahData('prepaid', infoPay.data);
															return res.send(msgReq+" "+msgValue);
														} else {
															let message = "";
															let rc = infoPay.rc;
															if (infoPay.data == "null" || infoPay.data == "") {
																req.logger.log('error', "ERRCODE:404 NOMOR METER/IDPEL BELUM TERSEDIA dest:"+dest);
																req.logger.log('error', infoPay);
																message = " ERRCODE:404 NOMOR METER/IDPEL BELUM TERSEDIA";
															} else {
																req.logger.log('error', infoPay.data+" dest:"+dest);
																message = infoPay.data;
															}
															return res.send(message+' IDPEL:'+b.dest+'@RC:'+rc+'@');
														}
													} else {
														req.logger.log('error', 'hasil request kosong gagal untuk pin:'+b.pin+" dest:"+dest+" product:"+b.product);
														req.logger.log('error', 'response dari getway kosong');
														return res.send('ERRCODE:203 REQUEST GAGAL, DATA KONTEN KOSONG ');
													}
												} else {
													req.logger.log('error', "ERRCODE:001 REQUEST GAGAL, TERJADI KESALAHAN dest:"+dest)
													req.logger.log('error', err);
													return res.send("ERRCODE:001 REQUEST GAGAL, TERJADI KESALAHAN dest:"+dest);
												}
											});
										} else {
											req.logger.log('error', "ERRCODE:001 REQUEST GAGAL, TERJADI KESALAHAN dest:"+dest)
											return res.send("ERRCODE:001 REQUEST GAGAL, TERJADI KESALAHAN");
										}
									})
									.catch(async (err) => {
										console.log('ERROR 1', err);
										req.logger.log('error', "ERRCODE:001 REQUEST ULANG GAGAL, TERJADI KESALAHAN dest:"+dest)
										req.logger.log('error', err);
										return res.send("ERRCODE:001 REQUEST GAGAL, TERJADI KESALAHAN dest:"+dest);
									});
							});
						} else {
							let message = "";
							let rc = info.rc;
							if (info.data == "null" || info.data == "") {
								req.logger.log('error', "ERRCODE:404 NOMOR METER/IDPEL BELUM TERSEDIA dest:"+dest);
								req.logger.log('error', info);
								message = " ERRCODE:404 NOMOR METER/IDPEL BELUM TERSEDIA";
							} else {
								req.logger.log('error', info.data+" dest:"+dest);
								message = info.data;
							}
							return res.send(message+' IDPEL:'+b.dest+'@RC:'+rc+'@');
						}
					})
					.catch(async (err) => {
						req.logger.log('error', "ERRCODE:001 REQUEST GAGAL, TERJADI KESALAHAN dest:"+dest)
						req.logger.log('error', err);
						return res.send("ERRCODE:001 REQUEST GAGAL, TERJADI KESALAHAN dest:"+dest);
					});
			} 
			//  INI UNTUK REQUEST PRODUCT YG ADA DI VARIBEL ARR_KODE
			else if (cek != -1) {
				let options = await getOptions(req, JSESSIONID, b);
				let msgReq = await getMsgReq(product);
				reqData(options)
					.then(async (info) => {
						if (info.rc == "00") {
							let output = await pilahData(product, info.data);
							req.logger.log('info', "request inquery/payment sukses dest:"+dest);
							req.logger.log('info', output);
							return res.send(msgReq+" "+output);
						} else if (info.rc == "999") {
							rollback(req, b, async (err, resOtpions) => {
								if (err) return res.send(err);
								reqData(resOtpions)
									.then(async (output) => {
										if (output.rc == "00") {
											let outRetry = await pilahData(product, output.data);
											req.logger.log('info', "request ulang sukses dest:"+dest);
											req.logger.log('info', outRetry);
											return res.send(msgReq+" "+outRetry);
										} else {
											let message = "";
											let rc = info.rc;
											if (output.data == "null" || output.data == "") {
												req.logger.log('error', "ERRCODE:404 NOMOR METER/IDPEL BELUM TERSEDIA dest:"+dest);
												req.logger.log('error', output);
												message = " ERRCODE:404 NOMOR METER/IDPEL BELUM TERSEDIA";
											} else {
												req.logger.log('error', output.data+" dest:"+dest);
												message = output.data;
											}
											return res.send(message+' IDPEL:'+b.dest+'@RC:'+rc+'@');
										}
									})
									.catch((err) => {
										console.log('ERROR 1', err);
										req.logger.log('error', "ERRCODE:001 REQUEST ULANG GAGAL, TERJADI KESALAHAN dest:"+dest)
										req.logger.log('error', err);
										return res.send("ERRCODE:001 REQUEST GAGAL, TERJADI KESALAHAN dest:"+dest);
									})
							});
						} else {
							let message = "";
							let rc = info.rc;
							if (info.data == "null" || info.data == "") {
								req.logger.log('error', "ERRCODE:404 NOMOR METER/IDPEL BELUM TERSEDIA dest:"+dest);
								req.logger.log('error', info);
								message = " ERRCODE:404 NOMOR METER/IDPEL BELUM TERSEDIA";
							} else {
								req.logger.log('error', info.data+" dest:"+dest);
								message = info.data;
							}
							return res.send(message+' IDPEL:'+b.dest+'@RC:'+rc+'@');
						}
					})
					.catch((err) => {
						console.log('ERROR 1', err);
						req.logger.log('error', "ERRCODE:001 REQUEST GAGAL, TERJADI KESALAHAN dest:"+dest)
						req.logger.log('error', err);
						return res.send("ERRCODE:001 REQUEST GAGAL, TERJADI KESALAHAN dest:"+dest);
					});
			}
			// INI UNTUK REQUEST PAYMENT PLN PREPAID 
			else if (arr_prod.length > 1) {
				let product2 = arr_prod[0];
				let kode_product = arr_prod[1];
				let tag = arr_prod[2];
				b['product'] = product2;
				b['kode'] = kode_product;
				b['tag'] = tag;
				let options = await getOptions(req, JSESSIONID, b);
				let msgReq = await getMsgReq(product2, tag);
				reqData(options)
					.then(async (info) => {
						if (info.rc == "00") {
							let output = await pilahData(product2, info.data);
							req.logger.log('info', "request inquery/payment sukses dest:"+dest);
							req.logger.log('info', output);
							return res.send(msgReq+" "+output);
						} else if (info.rc == "999") {
							rollback(req, b, async (err, resOtpions) => {
								if (err) return res.send(err);
								reqData(resOtpions)
									.then(async (output) => {
										if (output.rc == "00") {
											let outRetry = await pilahData(product2, output.data);
											req.logger.log('info', "request ulang sukses dest:"+dest);
											req.logger.log('info', outRetry);
											return res.send(msgReq+" "+outRetry);
										} else {
											let message = "";
											let rc = info.rc;
											if (output.data == "null" || output.data == "") {
												req.logger.log('error', "ERRCODE:404 NOMOR METER/IDPEL BELUM TERSEDIA dest:"+dest);
												req.logger.log('error', output);
												message = " ERRCODE:404 NOMOR METER/IDPEL BELUM TERSEDIA";
											} else {
												req.logger.log('error', output.data+" dest:"+dest);
												message = output.data;
											}
											return res.send(message+' IDPEL:'+b.dest+'@RC:'+rc+'@');
										}
									})
									.catch((err) => {
										console.log('ERROR 1', err);
										req.logger.log('error', "ERRCODE:001 REQUEST ULANG GAGAL, TERJADI KESALAHAN dest:"+dest)
										req.logger.log('error', err);
										return res.send("ERRCODE:001 REQUEST GAGAL, TERJADI KESALAHAN dest:"+dest);
									})
							});
						} else {
							let message = "";
							let rc = info.rc;
							if (info.data == "null" || info.data == "") {
								req.logger.log('error', "ERRCODE:404 NOMOR METER/IDPEL BELUM TERSEDIA dest:"+dest);
								req.logger.log('error', info);
								message = " ERRCODE:404 NOMOR METER/IDPEL BELUM TERSEDIA";
							} else {
								req.logger.log('error', info.data+" dest:"+dest);
								message = info.data;
							}
							return res.send(message+' IDPEL:'+b.dest+'@RC:'+rc+'@');
						}
					})
					.catch((err) => {
						console.log('ERROR 1', err);
						req.logger.log('error', "ERRCODE:001 REQUEST GAGAL, TERJADI KESALAHAN dest:"+dest)
						req.logger.log('error', err);
						return res.send("ERRCODE:001 REQUEST GAGAL, TERJADI KESALAHAN dest:"+dest);
					})
			} else {
				req.logger.log('error', "PRODUCT BELUM TERSEDIA  product : "+product);
				return res.send('ERRCODE:404 PRODUCT BELUM TERSEDIA  product : '+product);
			}
		}
	}

	const rollback = async (req, params, callback) => {
		let dest = params.dest;
		let password = params.password;
		let username = params.pin;
		let tmpUserLogin = req.generate.enkripsi(username+'~9e0d031d6908a681~1.0.0~andro');
		let tmpPswdLogin = md5(password);
		let tmpSogokLogin = md5(tmpUserLogin+"SUBHANALLAH-ALHAMDULILLAH");
		// let URLlogin = ApiUrl+"/loginserpul?act=login&usr="+tmpUserLogin+"&psw="+tmpPswdLogin+"&sogok="+tmpSogokLogin;
		let URLlogin = ApiUrl+"/logingatewayserpul?act=login&usr="+encodeURI(tmpUserLogin)+"&psw="+tmpPswdLogin+"&sogok="+tmpSogokLogin;
		let optionsLogin = {
			url: URLlogin,
			method : 'GET',
		}
		console.log(URLlogin);
		console.log('login ulang');
		request(optionsLogin, async (err, response, body) => {
			if (!err && response.statusCode == 200) {
				let infoLogin = JSON.parse(body);
				console.log('hasil dari rollback');
				console.log(infoLogin);
				if (infoLogin.rc == "00") {
					let setcookie = response.headers["set-cookie"][0];
					let query = querystring.parse(setcookie);
					let JSESSIONID = query.JSESSIONID;
					let pecah = JSESSIONID.split(';');
					let session = pecah[0];
					console.log('get session ulang ===> '+ session);
					req.logger.log('info', 'request login ulang sukses pin : '+params.pin+' dest : '+dest+' JSESSIONID : '+ session+" untuk product : "+params.product);
					req.db.update('jsessionid', {_id : username }, { $set : { JSESSIONID : session }} , (err, rows) => {
					});
					let optionsRequest = await getOptions(req, session, params);
					if (params.flag) {
						return callback(null ,{ options : optionsRequest, JSESSIONID : session});
					} else {
						return callback(null ,optionsRequest);
					}
				} else {
					req.logger.log('error', 'request login ulang gagal');
					req.logger.log('error', infoLogin.data+" pin:"+params.pin+" dest:"+dest+" password:"+params.password);
					return callback(infoLogin.data+" dest:"+dest , null);
				}
			} else {
				req.logger.log('error', 'request login ulang gagal dest:'+dest);
				req.logger.log('error', err);
				return callback('ERRCODE:000 REQUEST GAGAL, SILAHKAN DIULANGI dest:'+dest, null);
			}
		});
	}

	const pilahData = async (product, responseData) => {
		return new Promise (async (resolve, reject) => {
			let hasil = "";
			switch (product) {
				case "cekpln" :
					hasil = await inqPost(responseData);
				break;
				case "pln" :
					hasil = await payPost(responseData);
				break;
				case "ceknontaglis" :
					hasil = await inqNontaglis(responseData);
				break;
				case "nontaglis" :
					hasil = await payNontaglis(responseData);
				break;
				case "cekprepaid" :
					hasil = await inqPre(responseData);
				break;
				case "prepaid" :
					hasil = await payPre(responseData);
				break;

				// NEW 
				case "cektelcopre" :
					hasil = await inqTelcoPre(responseData);
				break;
				case "cektelcopos" :
					hasil = await inqTelcoPos(responseData);
				break;
				case "cekbpjskes" :
					hasil = await inqBpjsKes(responseData);
				break;
				case "bpjskes" :
					hasil = await payBpjsKes(responseData);
				break;
				case "cekfinance" :
					hasil = await inqFinace(responseData);
				break;
				case "finance" :
					hasil = await payFinace(responseData);
				break;
				case "cektvcable" :
					hasil = await inqTvCable(responseData);
				break;
				case "tvcable" :
					hasil = await payTvCable(responseData);
				break;
				case "cekpdam" :
					hasil = await inqPDAM(responseData);
				break;
			}
			return resolve(hasil);
		});
	}

	// PARSING INQUERY
	const inqTelcoPre = async (responseData) => {
		return new Promise(async (resolve, reject) => {
			let hasil = "";
			let arr = responseData.split('!');
			_.each(arr, async (item) => {
				let pisah = item.split("|");
				hasil += item+"@";
			});
			return resolve(hasil);
		});
	}
	
	const inqTelcoPos = async (responseData) => {
		return new Promise(async (resolve, reject) => {
			let hasil = "";
			let arr = responseData.split("|");
			hasil += "IDPEL : "+arr[0]+"@";
			hasil += "NAMA : "+arr[1]+"@";
			hasil += "PROVIDER : "+arr[4]+"@";
			hasil += "RP TAG :"+arr[3]+"@";
			hasil += "RP ADM : "+arr[6]+"@";
			hasil += "TOTAL TAGIHAN : "+ arr[7]+"@";
			return resolve(hasil);
		});
	}

	const inqPDAM = async (responseData) => {
		return new Promise(async (resolve, reject) => {
			let hasil = "";
			let arr = responseData.split("|");

			return resolve(responseData);
		});
	}

	const inqTvCable = async (responseData) => {
		return new Promise(async (resolve, reject) => {
			let hasil = "";
			let arr = responseData.split("|");
			hasil +="ID KONTAK : "+arr[0]+"@";
			hasil +="NAMA : "+arr[1]+"@";
			hasil +="SM : "+arr[2]+"@";
			hasil +="LAYANAN : "+arr[3]+"@";
			hasil +="RP TAG : "+arr[4]+"@";
			hasil +="RP ADMIN : "+arr[5]+"@";
			hasil +="TOTAL TAGIHAN : "+arr[7]+"@";
			return resolve(hasil);
		});
	}

	const inqFinace = async (responseData) => {
		return new Promise(async (resolve, reject) => {
			let hasil = "";
			let arr = responseData.split("|");
			hasil +="ID KONTAK : "+arr[0]+"@";
			hasil +="NAMA : "+arr[1]+"@";
			hasil +="ANGGURAN KE : "+arr[5]+"@";
			hasil +="LAYANAN : "+arr[4]+"@";
			hasil +="RP ADM : "+arr[6]+"@";
			hasil +="TOTAL TAGIHAN : "+arr[7]+"@";
			return resolve(hasil);
		});
	}

	const inqBpjsKes = async (responseData) => {
		return new Promise(async (resolve, reject) => {
			let hasil = "";
			let arr = responseData.split("|");
			hasil +="NO ACCOUNT : "+ arr[0]+"@";
			hasil +="PERIODE : "+arr[1]+"@";
			hasil +="NAMA : "+arr[2]+"@";
			hasil +="RP TAG : "+arr[3]+"@";
			hasil +="RP ADM : "+arr[5]+"@";
			hasil +="TOTAL TAGIHAN : "+arr[7]+"@";
			return resolve(hasil);
		});
	}

	const inqPre = async (responseData) => {
		return new Promise (async (resolve, reject) => {
			let hasil = "";
			let arr = responseData.split('|');
			hasil +="METER : "+ arr[0]+"@";
			hasil +="IDPEL : "+ arr[1]+"@";
			hasil +="NAMA : "+ arr[2]+"@";
			hasil +="TARIF/DAYA : "+ arr[4]+"@";
			hasil +="RP ADM : "+ arr[7]+"@";
			return resolve(hasil);
		})
	}

	const inqNontaglis = async (responseData) => {
		return new Promise(async (resolve, reject) => {
			let hasil = "";
			let arr = responseData.split('|');
			hasil +="IDPEL : "+ arr[0]+"@";
			hasil +="NAMA : "+ arr[1]+"@";
			hasil +="RP TAG : "+ arr[4]+"@";
			hasil +="ADM : "+ arr[3]+"@";
			hasil +="TOTAL TAGIHAN : "+ arr[7]+"@";
			return resolve(hasil);
		});
	}

	const inqPost = async (responseData) => {
		return new Promise(async (resolve, reject) => {
			let hasil = "";
			let data = responseData.split('|');
			let rptag = data[7] - data[4];
			hasil += "IDPEL : "+data[0];
			hasil += "@NAMA : "+data[1];
			hasil += "@BLN : "+data[2];
			hasil += "@JML : "+data[3];
			hasil += "@RP TAG : "+rptag;
			hasil += "@ADM : "+data[4];
			hasil += "@SM : "+data[5];
			hasil += "@TOTAL TAGIHAN : "+data[7]+"@"
			return resolve(hasil);
		});
	}
	// END PARSING

	// PARSING PAYMENT
	const payTvCable = async (responseData) => {
		return new Promise(async (resolve, reject) => {
			let hasil = "";
			let arr = responseData.split("|");
			hasil +="ID KONTAK : "+arr[2]+"@";
			hasil +="NAMA : "+arr[3]+"@";
			hasil +="SM : "+arr[5]+"@";
			hasil +="INFO : "+arr[10]+"@";
			hasil += "NOREF : "+arr[11]+"@";
			hasil += "SALDO : "+arr[21]+"@";
			return resolve(hasil);
		});
	}

	const payFinace = async (responseData) => {
		return new Promise(async (resolve, reject) => {
			let hasil = "";
			let arr = responseData.split("|");
			hasil +="ID KONTAK : "+arr[2]+"@";
			hasil +="NAMA : "+arr[5]+"@";
			hasil += "TGL REG : "+arr[11]+"@";
			hasil +="INFO : "+arr[18]+"@";
			hasil += "NOREF : "+arr[19]+"@";
			hasil += "RP TAG : "+arr[17]+"@";
			hasil += "SALDO : "+arr[26]+"@";
			return resolve(hasil);
		});
	}

	const payBpjsKes = async (responseData) => {
		return new Promise(async (resolve, reject) => {
			let hasil = "";
			let arr = responseData.split('|');
			hasil +="NO ACCOUNT : "+arr[4]+"@";
			hasil +="NAMA : "+arr[3]+"@";
			hasil +="PERIODE : "+ arr[6]+"@";
			hasil += "LAYANAN : "+arr[19]+"@";
			hasil +="INFO : "+arr[10]+"@";
			hasil += "NOREF : "+arr[11]+"@";
			hasil += "RP TAG : "+arr[7]+"@";
			hasil += "RP ADM : "+arr[8]+"@";
			hasil += "TOTAL TAGIHAN : "+arr[9]+"@";
			hasil += "SALDO : "+arr[21]+"@";
			return resolve(hasil);
		});
	}

	const payPre = async (responseData) => {
		return new Promise (async (resolve, reject) => {
			let hasil = "";
			let arr = responseData.split('|');
			let rptag = Number(arr[16]) - Number(arr[17]);
			let token = arr[14];
			let newToken = token.toString().match(/.{4}/g).join('-');

			hasil +="NOMETER :"+arr[2]+"@";
			hasil +="IDPEL :"+arr[8]+"@";
			hasil +="NAMA :"+arr[5]+"@";
			hasil +="TARIF :"+arr[6]+"@";
			hasil +="DAYA :"+arr[7]+"@";
			hasil +="TOKEN :"+newToken+"@";
			// hasil +="TOKEN :"+arr[14]+"@";
			hasil +="JML KWH :"+arr[18].replace(/#/g,",")+"@";
			hasil +="RP TOKEN :"+arr[23].replace(/#/g,",")+"@";
			hasil +="PJU :"+arr[19].replace(/#/g,",")+"@";
			hasil +="ANGSURAN :"+arr[20].replace(/#/g,",")+"@";
			hasil +="MATERAI :"+arr[3].replace(/#/g,",")+"@";
			hasil +="PPN :"+arr[4].replace(/#/g,",")+"@";

			hasil +="NOREF :"+arr[22]+"@";
			hasil +="HARGA :"+rptag+"@";
			hasil +="RP ADM :"+arr[17]+"@";
			hasil += "TOTAL TAGIHAN : "+arr[16]+"@";
			hasil += "SALDO : "+arr[28]+"@";
			return resolve(hasil);
		});
	}

	const payNontaglis = async (responseData) => {
		return new Promise (async (resolve, reject) => {
			let hasil = "";
			let arr = responseData.split('|');
			let total_tagihan = Number(arr[16]) + Number(arr[17]);
			hasil += "NOREG : "+arr[2]+"@";
			hasil += "IDPEL : "+arr[3]+"@";
			hasil += "NAMA : "+arr[5]+"@";
			hasil += "TGL REG : "+arr[6]+"@";
			hasil += "LAYANAN : "+arr[24]+"@";
			hasil += "INFO : "+arr[25]+"@";
			hasil += "TLP UNIT PLN : "+arr[26]+"@";
			hasil += "NOREF : "+arr[14]+"@";
			hasil += "HARGA : "+arr[16]+"@";
			hasil += "RP ADM : "+arr[17]+"@";
			hasil += "TOTAL TAGIHAN : "+total_tagihan+"@";
			hasil += "SALDO : "+arr[28]+"@";
			return resolve(hasil);
			// hasil += "TGL BAYAR : "+arr[15]+"@";
		});
	}

	const payPost = async (responseData) => {
		return new Promise (async (resolve, reject) => {
			let hasil = "";
			let arr = responseData.split('|');
			let total_tagihan = Number(arr[16]) + Number(arr[17]);
			hasil += "IDPEL : "+arr[2]+"@";
			hasil += "NAMA : "+arr[5]+"@";
			hasil += "BLTH : "+arr[3]+"@";
			hasil += "TARIF : "+arr[6]+"@";
			hasil += "DAYA : "+arr[7]+"@";
			// hasil += "LAYANAN : "+arr[24]+"@";
			// hasil += "TGL BAYAR : "+arr[15]+"@";
			hasil += "INFO : "+arr[25]+"@";
			hasil += "TLP UNIT PLN : "+arr[26]+"@";
			hasil += "NOREF : "+arr[14]+"@";
			hasil += "RP TAG : "+arr[16]+"@";
			hasil += "RP ADM : "+arr[17]+"@";
			hasil += "TOTAL TAGIHAN : "+total_tagihan+"@";
			hasil += "SALDO : "+arr[28]+"@";
			return resolve(hasil);
		});
	}
	// END PARSING

	const reqData = async (options) => {
		return new Promise(async ( resolve, reject) => {
			try {
				request(options, (err, response, body) => {
					console.log(err);
					if (!err && response.statusCode == 200) {
						console.log('true');
						console.log(body);
						if (body) {
							let info = JSON.parse(body);
							return resolve(info);
						} else {
							return reject("ERRCODE:203 REQUEST GAGAL, DATA KONTEN KOSONG");
						}
					} else {
						return reject(err);
					}
				});
			} catch (err) {
				return reject(err);
			}
		});
	}

	const getOptions = async (req ,JSESSIONID ,params) => {
		return new Promise (async (resolve, reject) => {
			let tmpIsi = "";
			let jnsReq = await getJnsRequest(params.product);
			let ArrayID = await getArrayID (params, params.product);
			tmpIsi = jnsReq+'~'+ArrayID+'~'+JSESSIONID;
			console.log('tmpIsi ===> '+ tmpIsi);
			tmpIsi = req.generate.enkripsi(tmpIsi);
			let URL = ApiUrl+'/requestserpul?isi='+encodeURI(tmpIsi)+'&sogok='+JSESSIONID;
			let cookie = "JSESSIONID="+JSESSIONID+";";
			let options = {
				url : URL,
				method : 'GET',
				headers: {
					'Cookie' : cookie,
				}
			}
			return resolve(options);
		});
	}

	// GET JENIS REQUEST LAYANAN PRODUK
	const getJnsRequest = async (product) => {
		return new Promise (async (resolve, reject) => {
			let jnsReq = "";
			switch(product.toLowerCase()) {
				case "cekpln" :
					jnsReq = "inq~post";
				break;
				case "pln" :
					jnsReq = "pay~post";
				break;
				case "ceknontaglis" :
					jnsReq = "inq~nebill";
				break;
				case "nontaglis" :
					jnsReq = "pay~nebill";
				break;
				case "cekprepaid" : 
					jnsReq = "inq~pre";
				break;
				case "prepaid" :
					jnsReq = "pay~prepaid";
				break;
				// NEW
				case "telcopre" :
					jnsReq = "pay~multipre";
				break;
				case "cektelcopre" : 
					jnsReq = "inq~multipre";
				break;
				case "telcopos" :
					jnsReq = "pay~multipos";
				break; 
				case "cektelcopos" :
					jnsReq = "inq~multipos";
				break;
				case "cekbpjskes" :
					jnsReq = "inq~bpjskes";
				break;
				case "bpjskes" :
					jnsReq = "pay~bpjskes";
				break;
				case "cekfinance" :
					jnsReq = "inq~finance";
				break;
				case "finance" :
					jnsReq = "pay~finance";
				break;
				case "cektvcable" :
					jnsReq = "inq~tv";
				break;
				case "tvcable" :
					jnsReq = "pay~tv";
				break;
				case "cekpdam" :
				jnsReq = "inq~pdam";
				break;
				// END NEW
			}
			return resolve(jnsReq);
		});
	}
	// END

	// GET ArrayID
	const getArrayID = async (params, product) => {
		return new Promise (async (resolve, reject) => {
			let ArrayID = "";
			let dest = params.dest;
			let kode = params.kode;
			switch(product.toLowerCase()) {
				case "cekpln" :
					ArrayID = dest+'!0!0';
				break;
				case "pln" :
					ArrayID = dest;
				break;
				case "ceknontaglis" :
					ArrayID = dest;
				break;
				case "nontaglis" :
					ArrayID = dest;
				break;
				case "cekprepaid" :
					ArrayID = dest+'!0';
				break;
				case "prepaid" :
					let denom = kode.replace('plnpre','');
					denom +="000";
					ArrayID = dest+denom+'!BARU!0'; // jika ada pemisah ! di kode produk
					// ArrayID = dest+"!"+denom+'!BARU!0'; // jika tidak ada pemisah ! di kode produk
				break;

				// NEW
				case "telcopre" :
					let arr = kode.split('*');
					let kode1 = arr[0];
					let kode2 = arr[1];
					let tag = params.tag;
					ArrayID = dest+"!"+kode1+"!"+kode2+"!"+tag;
					console.log('ArrayID ===> '+ ArrayID);
				break;
				case "cektelcopre" :
					ArrayID = dest+"!"+kode;
					console.log("ArrayID ==> "+ ArrayID);
				break;
				case "cektelcopos" :
					let kode_area = dest.substr(0,4);
					let value = dest.substr(4, dest.length);
					ArrayID = kode_area+"!"+value+"!"+kode;
				break;
				case "cekbpjskes" :
					ArrayID = dest+'!'+kode;
				break;
				case "bpjskes" :
					ArrayID = dest+'!'+kode;
				break;
				case "cekfinance" :
					// ArrayID = dest+'!86501';
					ArrayID = dest+'!'+kode;
				break;
				case "finance" :
					// ArrayID = dest+"!5001";
					ArrayID = dest+"!"+kode;
				break;
				case "cektvcable" :
					ArrayID = dest+"!"+kode;
				break;
				case "tvcable" :
					ArrayID = dest;
				break;
				case "cekpdam" :
					ArrayID = dest+"!"+kode+"!0!0";
				break;
				// END NEW
			}
			return resolve(ArrayID);
		});
	}
	// END

	// GET MESSAGE SUKSES INQ/PAY
	const getMsgReq = async (product, label) => {
		return new Promise(async (resolve, reject) => {
			let tag = label || "";
			let msg = "";
			switch (product.toLowerCase()) {
				case "cekpln" :
					msg = "CEK TAGIHAN PLN SUKSES";
				break;
				case "pln" :
					msg = "PEMBAYARAN TAGIHAN PLN SUKSES";
				break;
				case "ceknontaglis" :
					msg = "CEK TAGIHAN NONTAGLIS SUKSES";
				break;
				case "nontaglis" :
					msg = "PEMBAYARAN TAGIHAN NONTAGLIS SUKSES";
				break;
				case "cekprepaid" :
					msg = "CEK LISTRIK PREPAID SUKSES";
				break;
				case "prepaid" :
					msg = "PEMBELIAN LISTRIK PREPAID SUKSES";
				break;
				case "cekbpjskes" :
					msg = "CEK TAGIHAN BPJS KESEHATAN SUKSES";
				break;
				case "bpjskes" :
					msg = "PEMBAYARAN TAGIHAN BPJS KESEHATAN SUKSES";
				break;
				case "cekfinance" :
					tag = tag.toUpperCase();
					msg = "CEK TAGIHAN FINANCE "+tag+" SUKSES";
				break;
				case "finance" :
					tag = tag.toUpperCase();
					msg = "PEMBAYARAN TAGIHAN FINANCE "+tag+" SUKSES";
				break;
				case "cektvcable" :
					tag = tag.toUpperCase();
					msg = "CEK TAGIHAN TV CABLE "+tag+" SUKSES";
				break;
				case "tvcable" :
					tag = tag.toUpperCase();
					msg = "PEMBAYARAN TAGIHAN TV CABLE "+tag+" SUKSES";
				break;
				case "cekpdam" :
					tag = tag.toUpperCase();
					msg = "CEK TAGIHAN PDAM "+tag+" SUKSES";
				break;
				case "cektelcopos" :
					msg = "CEK TAGIHAN TELCO POSTPAID SUKSES";
				break;
				case "cektelcopre" :
					msg = "CEK TAGIHAN TELCO PREPAID SUKSES";
				break;
				default :
					msg = "KODE PRODUCT NOT FOUND ATAU TIDAK DI TEMUKAN";
				break;
			}
			return resolve(msg);
		});
	}
	// END
}

module.exports = Serpul;
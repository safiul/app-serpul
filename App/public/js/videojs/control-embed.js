var plugins = {};
var player = videojs("my-video");

function downloadThis() {
	var selector = $("#wrapper-video");
	var id = selector.attr("data-id");
	var title = selector.attr("data-name");

	$.ajax({
		url : "/downloadme/" + id,
		type : "POST",
		data : {title : title, type : "embed"},
		beforeSend : function() {
			console.log("Memindai File")
		},
		success : function(msg) {
			if (msg.status == 'success') {
				if (msg.action == 'play') {
					console.log("success");
				}
			}
			else {
				$.error("There is an error from server");
			}
		}
	})
}

function initWatermark() {
	var file = $("#wrapper-video").attr("data-logo") || "";
	player.watermark({
		"file" : file,
		"xpos": 100,
		"ypos": 0
    })
}

function initResolution() {
	player.videoJsResolutionSwitcher({
		default : 2,
		dynamicLabel : true
	});
	$(".vjs-audio-button").remove();
}

function initHotKeys() {
	player.hotkeys({
		volumeStep : 0.1,
		seekStep : 5
	})
}
function initThumbnail() {
	player.thumbnails({
		width : 120,
		height : 120,
		source : $("#wrapper-video").attr("data-source-thumbnail") + "/"
	})
}
initThumbnail();
initWatermark();
initResolution();
initHotKeys();
var watchdojoEmbed;
var width = '100%';
var height = '420';
var type = 'ihik2';
var key = $('meta[name = "watchdojo-embed"]').attr('data-key') || null;
var host = window.location.hostname;
var panelButton = "";
var idnya = "";
// console.log('iki key.ne '+ key);
var elementLoading = "<div class = 'row' style=\"background-color:#263238; width:100%; height:100%; top:0;\"><h4 style=\"margin:0px; text-align:center; text-transform:uppercase; padding-top:40%; font-size:20px; color:#FFF;\">Loading ....</h4></div>";

// REMOVE IKLAN
function removeIklan() {
	$("#shdm_ads").fadeOut();
}

function RunJwPlayer (selector, cover , title ,  sub  , buttonObj , film, options, token) {
	if (options != undefined) {
		width = options.width || width;
		height = options.height || height;
		type = options.type || type;
	}
	
	var namaKey = Object.keys(film);
	// console.log("nameKey", namaKey);
	var arryType = ['picasa','openload','videomega','youtube','ihik2'];
	// console.log(type, namaKey.indexOf(type));
	if (namaKey.indexOf(type) != -1) {
		type = namaKey[namaKey.indexOf(type)];
	} else {
		if (type != 'ihik2' || namaKey.indexOf("ihik2") == -1) {
			type = namaKey[0];
		} else {
			type = type;
		}
	}
	
	if (type == 'picasa') {
		jwplayer.key = "QgvdKpoES/cVT96vc7NNyOHaemmnZdEEcLbfi+KQXd4HzLSUtQImuNkVWGnPP1VY";
		watchdojoEmbed = jwplayer(selector).setup({
			playlist:[{
				image: cover,
				sources : film.picasa,
				tracks : sub,
				title : title
			}],
			sharing : {link:""},
			startparam : "begin",
			displaytitle :false,
			width : width,
			logo : {
				// file : "http://steamhdmovies.com/images/shdm-wm.png",
				file : options.logo,
				link : "http://" + options.site,
				position : 'top-right',
				linktarget : "_blank"
			},
			height : height,
			captions : {
				back :false,
				fontsize : 20
			},
			modes: [
				{type: 'html5'},
				{type: 'flash', src: "jwplayer.flash.swf"},
				{type: 'download'}
			]
		});

		// var elementIklan = "<div id=\"shdm_ads\" style=\"position:relative; width:100%; height:100%\">";
		// elementIklan += "<img src=\"http://pokerterpercaya.co/wp-content/uploads/2015/09/banner-kudapoker.gif\" style=\"display:block; top:20%; left:35%; position:absolute; z-index:9\"><br/>";
		// elementIklan += "<a href=\"#\" style=\"font-family:Arial; background-color:#F80; padding:.5em; display:block; top:20%; left:35%; position:absolute; z-index:10; cursor:pointer\" onclick=\"removeIklan()\">Close Ads</a>";
		// elementIklan += "</div>";
		// var t = setInterval(function() {
		// 	var jumlahDisplay = $("#" + selector + "_display").length;
		// 	if (jumlahDisplay != 0) {
		// 		$("#" + selector + "_display").prepend(elementIklan);
		// 		clearInterval(t);
		// 	}
		// })

		// watchdojoEmbed.onError(function(event) {
		// 	watchdojoEmbed.load("http://lintahdarat.com/mp4/deleted.mp4");
		// })
	} else if (type == 'youtube') {
		$('#'+selector).html('');
		var elementVideo = '<iframe width = '+width+' height = '+height+' src="http://www.youtube.com/embed/'+film.youtube+'" allowfullscreen="true" webkitallowfullscreen="true" mozallowfullscreen="true"></iframe>'
		$('#'+selector).html(elementVideo);
	} else if (type == 'openload'){
		$('#'+selector).html('');
		film.openload = film.openload.match("https://openload.co/embed/")?film.openload:'https://openload.co/embed/' + film.openload;
		var elementIframe = '<iframe width="100%" height="420" scrolling="no" frameborder="0" src="' + film.openload +'" allowfullscreen="true" webkitallowfullscreen="true" mozallowfullscreen="true"></iframe>';
		$('#'+selector).html(elementIframe);
	} else if (type == 'videomega'){
		$('#'+selector).html('');
		film.videomega = film.videomega.match("http://videomega.tv/view.php?ref=")?film.videomega:"http://videomega.tv/view.php?ref=" + film.videomega + "&width=700&height=430";
		var elementIframe = '<iframe width="100%" height="420" scrolling="no" frameborder="0" src="'+film.videomega+'" allowfullscreen="true" webkitallowfullscreen="true" mozallowfullscreen="true"></iframe>';
		$('#'+selector).html(elementIframe);
	} else if (type == 'ihik2'){
		$('#'+selector).html('');

		// console.log(film.ihik2)
		$("#" + selector).shdm_embed({
			url : film.ihik2
		})
		// $('#'+selector).html("Layanan Ihik2 Sedang Dalam Masa Perbaikan :)");
	} else {
		$("#" + selector).html("FAILED REQUEST URL");
	}

	if (buttonObj != null ) {
		var tombolHtml = '<div style = "margin-top : 10px;" id="panelButton">';
		buttonObj.forEach(function  (i){
			tombolHtml += '<button type="button" style="margin-top:10px; margin-right:10px" onclick = "PutarVideo(this,\''+i.type+'\',\''+i.id+'\', \'' + selector + '\')" class="btn btn-info btn-shdm" style = "margin-right : 10px;">'+i.label+'</button>';
		});
		var id_item = $('#'+selector).attr('data-embed') || idnya;
		title = title.replace(/'/g, '');
		var paramater = id_item +'-'+title;
		// var host 
		tombolHtml +='<button class="btn btn-info btn-watchdojo" style="margin-top:10px; margin-right:10px" onclick="redirectTo(\'http://steamhdmovies.com/download/' + paramater+ '\')"> Download </button> </a>';
		tombolHtml +='</div>';

		if (panelButton == "") {
			if ($('#'+selector).next("#panelButton").length == 0) {
				$('#'+selector).after(tombolHtml);
			}
		}
		else {
			$(panelButton).html(tombolHtml);
		}
	}
}

function RequestLink (url ,selector) {
	$.ajax({
		url : url,
		crossDomain : true,
		type : 'GET',
		// xhrFields: { withCredentials: true },
		// data : {user : }
		beforeSend : function() {
			$(selector).html(elementLoading);
			console.log("Loading");
		},
		success : function (msg) {
			if (msg.status == 'sukses') {
				// console.log(msg);
				var tokenIhik2 = msg.data.ihik2&&msg.data.ihik2.token;
				return RunJwPlayer($(selector).attr("id"), msg.data.cover , msg.data.title , msg.data.subtitle , msg.tombol , msg.data.video, { width : width , height : '420px' , type : type , logo : msg.data.logo, site : msg.data.site}, tokenIhik2);
			} else {
				$(selector).html(msg.msg);
				$.error(msg.msg);
			}
		},
		error : function (xhr) {
			$(selector).html('');
		}
	});
}

function robotEmbed(selector, callback) {
	var id_emdb = $(selector).attr("data-embed");
	var l = document.createElement("a");
	l.href = $($("script[src*='/watchdojo.js']")[0]).attr("src");
	var hostname = l.hostname;
	var port = window.location.port!=''?":" + window.location.port:"";

	var golekKunci = "http://"+hostname+":"+port+"/generateToken";
	console.log(golekKunci);
	var data = {
		// host : hostname,
		host : 'steamhdmovies.com',
		ref : hostname,
		idItem : id_emdb
	}

	$.ajax({
		url : golekKunci,
		type : "POST",
		data : data,
		success : callback,
		error : function(xhr, txtStatus, error) {
			if (error == "Not Found") {
				return callback({status : 404, msg : "You Must Using Embed source from steamhdmovies.com"})
			}
			else {
				return callback({status : 500, msg : "Terjadi Kesalahan Server"})
			}
		}
	})
}

$.fn.watchdojo = function(options) {
	var selector = this;
	var id_emdb = $(selector).attr("data-embed") || options.id_emdb;
	var link = $(selector).attr('data-link');
	$(selector).html(elementLoading);

	// EXCEPTION IMDB UNDEFINED
	if (id_emdb == undefined) {
		return $(selector).html("DATA FOR EMBED NOT VALID");
	}

	robotEmbed(selector, function(msg) {
		if (msg.status == 200) {
			if (options != undefined) {
				width = options.width || width;
				height = options.height || height;
				type = options.type || type;
				panelButton = options.panelButton || panelButton;
				var defaultnnya = ["openload","picasa","videomega","youtube","ihik2"]

				if (defaultnnya.indexOf(type) == -1) {
					type = "ihik2";
				}
				if (options.id_emdb != undefined) {
					id_emdb = options.id_emdb;
				}
			}
			$(selector).width(width).height(height);
			idnya = id_emdb;
			RequestLink('http://localhost:9000/embed/'+id_emdb+'/?key='+key+'&host='+host+'&token='+msg.data.token, selector);
			// RequestLink('http://steamhdmovies.com/embed/'+id_emdb+'/?key='+key+'&host='+host+'&token='+msg.data.token, selector);
		}
		else {
			return $(selector).html(msg.msg);
		}
	});

}

function PutarVideo (selector, type ,id, player) {
	$('#' + player).watchdojo({
		id_emdb : id,
		type : type,
		height : '420px'
	});
}

function redirectTo(url) {
	window.open(url, '_blank');
}


$.fn.shdm_embed = function(options) {
	var selector = $(this);	
	var iframeElement = $("<iframe>", {
		src : options.url || "http://steamhdmovies.com",
		frameborder : 0,
		scrolling : 'no',
		allowfullscreen : true,
		webkitallowfullscreen : true,
		mozallowfullscreen : true,
		width:'100%',
		height: '100%'		
	}).appendTo(selector);
}
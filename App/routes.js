process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0"; // Ignore invalid self-signed ssl certificate
module.exports = exports = function (app) {
	// HANDLER SERVER
	const SerpulHandler = require('./handler/server/serpul'), Serpul = new SerpulHandler();
	const IRSHandler = require('./handler/server/irs'), IRS = new IRSHandler();
	const ErrorHandler = require('./handler/error').errorHandler;

	app.get('/api/request/trx', Serpul.isAuth , Serpul.index);
	app.get('/api/request/irs', IRS.isAuth , IRS.index);
	
	app.use(ErrorHandler);
}